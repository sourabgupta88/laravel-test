<?php

namespace Tests\Feature;

use Tests\TestCase;
use Faker\Factory as Faker;

class ApiHandlerTest extends TestCase
{
    protected $testDataNAB,
    		  $headers;
	protected function setUp(): void 
	{
	   parent::setUp();
	   // set your headers here
	   $this->headers = [
	            'Authorization' => 'Bearer ' . $this->getBearerToken(),
	            'Accept' => 'application/json'
	        ];
	   $faker = Faker::create();
	   $this->testDataNAB = [
		      "card_name" => $faker->firstName(),
		      "card_number" => $faker->regexify('[0-9]{16}'),
		      "valid_until" => "04/2022",
		      "amount_to_transfer" => "1",
		      "button_type" => "pay_with_nab"
		];

	}

	protected function getBearerToken()
	{
	   return env('PHP_UNIT_TEST_KEY');
	}

	/**
     * A basic unit test example.
     *
     * @return void
     */
    public function testNab()
    {
        
        $response = $this->postJson('/submit', $this->testDataNAB, $this->headers);
    	$this->assertEquals(200, $response->status());
	    
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testNabFail()
    {
        $this->testDataNAB["amount_to_transfer"] = 0;
        $response = $this->postJson('/submit', $this->testDataNAB, $this->headers);
    	$this->assertEquals(406, $response->status());
    }

}

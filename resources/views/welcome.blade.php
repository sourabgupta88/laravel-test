@extends('layout.default')
@section('content')
    <div class="alert alert-success d-none">
    </div>
    <div class="alert alert-danger d-none">
    </div>
    <script type="text/javascript">
        const url = '{{ URL::asset('/submit') }}'
    </script>
    <div class="card">
        <div class="card-header text-center font-weight-bold">
          Laravel 8 - One Page Application Form Example
        </div>
        <div class="card-body">
          <form name="one-page-application" id="one-page-application" class="needs-validation" method="post" action="javascript:void(0)">
           @csrf
            <div class="form-group">
                 <label for="card_name">Credit Card Name</label>
                 <input type="text" id="card_name" name="card_name" class="form-control" required="">
            </div>    
            <div class="form-group">
                 <label for="card_number">Credit card number</label>
                 <input type="text" name="card_number" placeholder="16 digit card number" class="form-control" id="cc-number" required="" pattern="[0-9]{16}" data-value-missing="A credit card number is required" data-pattern-mismatch="The credit card number must be 16 digits">
                 <div class="invalid-feedback">
                 </div>
            </div>
            <div class="form-group"> 
                <label class="date-label" for="valid_until">Valid Until</label>
                <input class="form-control datepicker" id="date" name="valid_until" placeholder="MM/YYY" pattern="\d{1,2}/\d{4}" type="text"/>
            </div>
            <div class="form-group">
                <label for="amount_to_transfer">Amount To Transfer</label>
                <input type="number" id="amount_to_transfer" name="amount_to_transfer" class="form-control" required="">
            </div>
            <div class="form-group">
                <input type="hidden" name="button_type" value="">
                <input type="submit" name="pay_with_nab" onclick="this.form.button_type.value=this.name"  class="btn btn-primary ajaxSubmit" value="Pay with NAB"> <input onclick="this.form.button_type.value=this.name"   type="submit" class="btn btn-info ajaxSubmit" name="pay_with_anz" value="Pay with ANZ">
            </div>
          </form>
        </div>
    </div> 
@endsection
<!doctype html>
	<html>
	<x-header />
	<body>
	  <div class="container mt-4" id="app">
	    @yield('content')
	  </div>
	  <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
	  <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
	</body>
</html>
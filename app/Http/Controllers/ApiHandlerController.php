<?php

namespace App\Http\Controllers;
use App\Helpers\ApiHelper;
use Illuminate\Http\Request;
use App\Http\Requests\ApiHandlerRequest;
use Spatie\ArrayToXml\ArrayToXml;
use Mtownsend\XmlToArray\XmlToArray;

class ApiHandlerController extends Controller
{
    protected $apiHelper,
              $NABBaseUrl,
              $ANZBaseUrl,
              $NABMerchantId,
              $NABMerchantKey,
              $ANZMerchantId,
              $ANZMerchantKey,
              $apiTimeOut;

    public function __construct(
      ApiHelper $apiHelper
    )
    {
	    $this->NABBaseUrl = config('services.credit_card.NAB');
      $this->ANZBaseUrl = config('services.credit_card.ANZ');
      $this->NABMerchantId = config('services.credit_card.NABMerchantId');
      $this->NABMerchantKey = config('services.credit_card.NABMerchantKey');
      $this->ANZMerchantId = config('services.credit_card.ANZMerchantId');
      $this->ANZMerchantKey = config('services.credit_card.ANZMerchantKey');
      $this->apiTimeOut = config('services.credit_card.apiTimeOut', 30);
      $this->apiHelper = $apiHelper;
    }

    public function submit(ApiHandlerRequest $request){
      $validatedData = $request->validated();
      
      $rawPostJsonData = [
          "from" => [
                      "card_number" => $validatedData["card_number"],
                      "card_name" => $validatedData["card_name"],
                      "cvv" => "000",
                    ],
          "amount" => $validatedData["amount_to_transfer"]
      ];

      if($validatedData["button_type"] === "pay_with_nab"){
        $url = $this->NABBaseUrl;
        $merchant_id =  $this->NABMerchantId;
        $merchant_key =  $this->NABMerchantKey;
        $headers = ["Accept"=>"application/xml", "Content-Type"=>"application/xml"];
      }
      else{
        $url = $this->ANZBaseUrl;
        $merchant_id =  $this->ANZMerchantId;
        $merchant_key =  $this->ANZMerchantKey;
        $headers = ["Accept"=>"application/json", "Content-Type"=>"application/json"];
      }

      $collection = collect([$rawPostJsonData, ["merchant_id"=>$merchant_id,"merchant_key"=>$merchant_key]]);
      $collapsed = $collection->collapse();
      $rawPostJsonData = $collapsed->all();

      if($validatedData["button_type"] === "pay_with_nab"){
        $rawPostJsonData = ArrayToXml::convert($rawPostJsonData);
      }
      else{
        $rawPostJsonData = json_encode($rawPostJsonData);
      }

      $response = $this->apiHelper->postRequest($url, $rawPostJsonData, $this->apiTimeOut, $headers);
     
      if(is_array($response) && isset($response["Error"])){
          return response()->json([
          'status' => 406,
          'errors' => $response["Error"]
        ], 406);
      }
      else{
          if($validatedData["button_type"] === "pay_with_nab"){
            $data = XmlToArray::convert($response->getContents());
          }
          else{
            $data = json_decode($response);
          }
        
      } 
      return response()->json([
          'status' => 200,
          'message' => $data
        ]); 	
  }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Faker\Factory as Faker;
use Carbon\Carbon;
use Spatie\ArrayToXml\ArrayToXml;
use App\Helpers\ApiHelper;
use App\Http\Requests\ServiceTestHanlderRequest;

class ServiceTestController extends Controller
{

	protected  $request,
               $faker, 
               $apiHelper;
	public function  __construct(Request $request, ApiHelper $apiHelper){
		$this->request = $request;
		$this->faker = Faker::create();
        $this->apiHelper = $apiHelper;
	}

    public function submitAPI( ServiceTestHanlderRequest $request, $parameter){
    	 
        $returnArr = [
    		"from" => [
    			"card_number" => $request->input("from.card_number")
    		],
    		"amount" => $request->input("amount"),
    		"transaction_number" => $this->faker->regexify('[A-Za-z0-9]{7}'),
    		"transaction_date" => Carbon::createFromTimeStamp($this->faker->dateTimeBetween(now())->getTimestamp())->setTimeZone('UTC')->format( "Y-m-d H:i:s" )
    	];
    	
    	if($parameter === "anz"){

    		return json_encode($returnArr);
    	}
    	else{

    		return ArrayToXml::convert($returnArr);
    	}
    	
    	
    }
   
}

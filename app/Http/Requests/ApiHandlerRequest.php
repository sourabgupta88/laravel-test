<?php

namespace App\Http\Requests;

use LVR\CreditCard\CardExpirationMonth;
use Illuminate\Foundation\Http\FormRequest;
use Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class ApiHandlerRequest extends FormRequest
{
    public static $somethingWentWrong = 'Something Went Wrong. Try Again Later';
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = $this->all();
        $rule = ['card_name' => 'required',
                 'card_number' => ['required', 'digits:16'],
                 'expiration_year' => ['required', 'gt:'.(date("Y")-1)],
                 'expiration_month' => ['required', new CardExpirationMonth($request['expiration_year'])],
                 'amount_to_transfer' => ['required', 'gt:0'],
                 'button_type'  => ['required']
                ];
        return $rule;
    }
    
    protected function getValidatorInstance()
    {
        $request = $this->all();
        
        $validityArr = explode("/",$request["valid_until"]);
        if(!empty($validityArr[1])){
            $request["expiration_year"] = $validityArr[1];
        }
        if(!empty($validityArr[0])){
            $request["expiration_month"] = $validityArr[0];
        }

        $this->getInputSource()->replace($request);
        
        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }

    public function messages()
    {
        return [
        'button_type.required' => static::$somethingWentWrong,
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(["status"=>406,"errors"=>$validator->errors()->all()], 406)); 
    }

}

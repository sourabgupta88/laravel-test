<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Mtownsend\XmlToArray\XmlToArray;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class ServiceTestHanlderRequest extends FormRequest
{
    public function rules()
    {
        $rule = ['from.card_name' => 'required',
                 'from.card_number' => ['required', 'digits:16'],
                 'from.cvv' => ['required', 'digits:3'],
                 'merchant_id' => ['required'],
                 'merchant_key' => ['required'],
                 'amount' => ['required', 'gt:0']
                ];
        return $rule;
    }
    
    protected function getValidatorInstance()
    {
        $request = $this->all();
        if(!empty($this->parameter) && $this->parameter === "nab"){
            $request = XmlToArray::convert($this->getContent());
        } 
        
        $this->getInputSource()->replace($request);
        return parent::getValidatorInstance();
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()->all(), 406)); 
        //return $validator->errors();
    }
}

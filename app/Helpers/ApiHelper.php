<?php


namespace App\Helpers;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\Exceptions\ApiInvalidConnection;
use Illuminate\Support\Facades\Log;


class ApiHelper
{
    protected $apiTimeout = '15.0';

    public function postRequest($baseUrl, $rawPostJsonData, $apiTimeOut, $additionalheaders = [])
    {
        try {

            $client = new Client([
                'base_uri' => $baseUrl,
                'timeout'  => (empty($apiTimeOut)) ? $this->apiTimeout : $apiTimeOut,
            ]);

            $headers = [
              'Content-Type'     => 'application/json'
            ];
            $collection = collect([$headers, $additionalheaders]);
            $collapsed = $collection->collapse();
            $headers = $collapsed->all();
            
            $response = $client->post(
                '',
                [
                    'headers' => $headers,
                    'body' => $rawPostJsonData,
                ]
            );

            if ($response->getStatusCode() == 200)
            {
               
                return $response->getBody();
            }
        }
        catch (RequestException $e)
        {
            if($e->getCode() === 406){
                return (["Error"=> explode("response:",strip_tags($e->getMessage()))[1]]);
            }
            else{
                $message = env('APP_NAME'). ' '.env('APP_ENV'). ' error while fetching '.$baseUrl;
                Log::error($message, [$e->getMessage()]);
                throw new ApiInvalidConnection($e->getMessage());
            }
            
        }
        catch (Exception $e)
        {
            $message = env('APP_NAME'). ' '.env('APP_ENV'). ' error while fetching '.$baseUrl;
            Log::error($message, [$e->getMessage()]);
            throw new ApiInvalidConnection($e->getMessage());
        }
    }
   
}

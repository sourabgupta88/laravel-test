<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;

class ApiInvalidConnection extends Exception
{
    public function report()
    {
        Log::error('Invalid Api Connection');
    }
}

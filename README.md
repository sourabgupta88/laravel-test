
## IMPORTANT SETUP
  These keys should be added into .env file
- [NAB_SERVICE='{URL}/api/servicetest/nab']
- [ANZ_SERVICE='{URL}/api/servicetest/anz']
- [NABMerchantId=123]
- [NABMerchantKey='qwerty']
- [ANZMerchantId='456']
- [ANZMerchantKey='asdf']
- [apiTimeOut=30]
- [PHP_UNIT_TEST_KEY=1LmjBDP4GUIPqnrlshQ29ouXjCUCN8G1V4vTIsSIRUy2AM54PxomhHy10Vc3]



## TEST CASE Laravel

run below in command prompt 
vendor\bin\phpunit tests\Feature\ApiHandlerTest.php

